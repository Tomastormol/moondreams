package com.example.moondreams;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ServicesActivity extends AppCompatActivity {

    GridView gridView;
    ListView listView;
    ArrayAdapter<String> arrayAdapter;

    int precio;
    TextView txtTotalPrice;

    String textServices[] = {"Buscador", "50 productos", "100 productos", "200 productos", "Seo", "Login", "Compra internet", "Pagos", "Idioma", "Redes Sociales", "Fotos de la web", "D & H"};
    int icons[] = {R.drawable.buscador, R.drawable.productos, R.drawable.productos2, R.drawable.productos3, R.drawable.seo, R.drawable.login, R.drawable.tienda, R.drawable.pagos, R.drawable.idioma, R.drawable.share, R.drawable.camera, R.drawable.hosting };
    int prices[] = {50, 100, 200, 400, 50, 100, 100, 100, 100, 50, 100, 50};
    boolean checked = false;
    Service serviceList[] = new Service[12];
    Service service;
    List<String> serviceListAdd;

    boolean buscador = false;
    boolean productos = false;
    boolean productos1 = false;
    boolean productos2 = false;
    boolean seo = false;
    boolean login = false;
    boolean compra = false;
    boolean pagos = false;
    boolean idioma = false;
    boolean redes = false;
    boolean fotos = false;
    boolean dominio = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        for(int i=0;i<textServices.length;i++){
            service = new Service(textServices[i],icons[i],prices[i], checked);
            serviceList[i] = service;
        }

        for(int i=0;i<serviceList.length;i++){
            System.out.println(i + " " + serviceList[i].getName());
        }

        precio = Integer.parseInt(this.getIntent().getStringExtra("Precio"));
        txtTotalPrice = findViewById(R.id.txtTotalPrice);
        txtTotalPrice.setText(Integer.toString(precio));

        gridView = findViewById(R.id.gridViewServices);
        listView = findViewById(R.id.listViewServices);

        // Grid View
        GridAdapter adapter = new GridAdapter(ServicesActivity.this, serviceList);
        gridView.setAdapter(adapter);

        // List View
        serviceListAdd = new ArrayList<String>();
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, serviceListAdd);
        listView.setAdapter(arrayAdapter);

        // On click item GridView
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("me cago en deu: " + gridView.getItemAtPosition(i));
                if(!serviceList[i].isChecked()){
                    precio += serviceList[i].getPrice();
                    //gridView.getChildAt(i).setBackgroundResource(R.drawable.background_border_selected);
                    serviceListAdd.add(serviceList[i].getName() + "  -  " + serviceList[i].getPrice() + "€");
                    serviceList[i].setChecked(true);
                }else{
                    precio -= serviceList[i].getPrice();
                    //gridView.getChildAt(i).setBackgroundResource(R.drawable.background_border);
                    serviceListAdd.remove(serviceList[i].getName() + "  -  " + serviceList[i].getPrice() + "€");
                    serviceList[i].setChecked(false);
                }
                listView.setAdapter(arrayAdapter);
                txtTotalPrice.setText(Integer.toString(precio));
            }
        });
    }
}