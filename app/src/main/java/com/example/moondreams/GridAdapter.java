package com.example.moondreams;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class GridAdapter extends BaseAdapter {

    private Context context;
    private Service serviceList[];

    private LayoutInflater inflater;

    public GridAdapter(Context context, Service serviceList[]) {

        this.context = context;
        this.serviceList = serviceList;

    }

    @Override
    public int getCount() {
        return serviceList.length;
    }

    @Override
    public Object getItem(int i) {
        return serviceList[i].getName();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View gridView = view;

        if(view == null){

            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            gridView = inflater.inflate(R.layout.custom_layout, null);

        }

        ImageView icon = gridView.findViewById(R.id.icons);
        TextView services = gridView.findViewById(R.id.textServices);

        icon.setImageResource(serviceList[i].getPhoto());
        services.setText(serviceList[i].getName());

        return gridView;
    }
}
