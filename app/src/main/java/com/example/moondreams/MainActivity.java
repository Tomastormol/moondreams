package com.example.moondreams;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgPlantilla;
    ImageView imgMedida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgPlantilla = findViewById(R.id.imgPlantilla);
        imgMedida = findViewById(R.id.imgMedida);

        imgPlantilla.setOnClickListener(this);
        imgMedida.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        Intent intent = new Intent(MainActivity.this, ServicesActivity.class);

        switch (view.getId()) {

            case R.id.imgPlantilla: // Click en la imagen de plantilla
                intent.putExtra("Precio", "400");
                break;

            case R.id.imgMedida: // Click en la imagen de a medida
                intent.putExtra("Precio", "600");
                break;

        }

        startActivity(intent);
    }
}
