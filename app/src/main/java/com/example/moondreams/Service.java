package com.example.moondreams;

public class Service {

    String name = "";
    int photo = 0;
    int price = 0;
    boolean checked = false;

    public Service() {
    }

    public Service(String name, int photo, int price, boolean checked) {
        this.name = name;
        this.photo = photo;
        this.price = price;
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
